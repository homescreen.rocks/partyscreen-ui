import {Component, OnInit} from '@angular/core';
import fontawesome from '@fortawesome/fontawesome';
import {faInstagram} from "@fortawesome/fontawesome-free-brands";

fontawesome.library.add(faInstagram);

@Component({
    selector: 'app-info-text-view',
    templateUrl: './info-text-view.component.html',
    styleUrls: ['./info-text-view.component.css']
})
export class InfoTextViewComponent implements OnInit {

    constructor() {
    }

    ngOnInit() {
    }

}
