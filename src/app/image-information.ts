export class ImageInformation {
  public type: string;
  public url: string;
}
