import {Injectable, OnInit} from '@angular/core';

import {GenericMessage} from "../messages/generic-message";
import {Observable} from "rxjs/internal/Observable";
import {Subject} from "rxjs/internal/Subject";
import {Observer} from "rxjs/internal/types";
import {NewImage} from "../messages/new-image";
import {MessageItem} from "../classes/message-item";
import {environment} from "../../environments/environment";

@Injectable({
    providedIn: 'root'
})
export class WebsocketService {

    private static SERVER_URL = '/api/v1/socket/';

    private socket: WebSocket;
    private observable: Subject<GenericMessage> = new Subject<GenericMessage>();
    private observableTexts: Subject<MessageItem> = new Subject<MessageItem>();
    private observableOthers: Subject<GenericMessage> = new Subject<GenericMessage>();
    private socketObservable: Subject<MessageEvent>;

    constructor() {
        this.socket = new WebSocket('ws://' + (environment.apiServer ? environment.apiServer : window.location.host) + WebsocketService.SERVER_URL);
        this.initObservable();
        this.enableDebugLog();
    }

    public getOthers() {
        return this.observableOthers;
    }

    public getImages() {
        return this.observable;
    }

    public textMessages() {
        return this.observableTexts;
    }

    public sendMessage(message: GenericMessage) {
        this.socket.send(JSON.stringify(message));
    }

    private initObservable() {
        this.socketObservable = Observable.create(
            (observer: Observer<MessageEvent>) => {
                this.socket.onmessage = observer.next.bind(observer);
                this.socket.onerror = observer.error.bind(observer);
                this.socket.onclose = observer.complete.bind(observer);
                return this.socket.close.bind(this.socket);
            }
        );

        this.socketObservable.subscribe((ev: MessageEvent) => {
            if (ev.type === 'message') {
                let genericPayload: GenericMessage = JSON.parse(ev.data);
                switch (genericPayload.event) {
                    case 'NewImage':
                        let payload: NewImage = JSON.parse(ev.data);
                        //if (payload.payload[0].sourceUrl === null) {
                        //    this.observableTexts.next(payload.payload[0]);
                        //} else {
                            this.observable.next(payload);
                        //}
                        break;
                    default:
                        this.observableOthers.next(payload);
                }
            }
        });
    }

    private enableDebugLog(): void {
        let watch = this.observable.subscribe(event => {
            console.log(event);
        });
    }
}
